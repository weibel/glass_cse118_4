package com.nand.fitnesspower;

import android.app.Activity;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Chronometer;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener {

  private Chronometer mChronometer;
  private TextView mHint, mPushups, mPlank, mJumpingJacks, mCounter;

  private long mBase = 0;
  private boolean touchedGround = false;
  private boolean jumped = false;
  private long powerLevel = 0;
  private long display = 0;

  private long baseTime;

  private int pushupCount = 0;
  private int jumpingJackCount = 0;
  private int plankCount = 0;

  private long startTime;

  private int currentEvent = 0;
  SensorManager mSensorManager;
  private double accelZ, accelY;



    @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_mainapp);

    mChronometer = (Chronometer) findViewById(R.id.stopwatch);
    mHint = (TextView) findViewById(R.id.phrase_hint);
    mCounter = (TextView) findViewById(R.id.counter);
    mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    mPushups = (TextView) findViewById(R.id.pushupView);
    mPlank = (TextView) findViewById(R.id.plankView);
    mJumpingJacks = (TextView) findViewById(R.id.jjView);

        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY), SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_NORMAL);

        pushupCount = 0;
        mHint.setText("Pushup Count");
        mCounter.setText(Integer.toString(pushupCount));
  }

  /**
   * Handle the tap event from the touchpad.
   */
  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    currentEvent++;
    if(currentEvent > 4){
        currentEvent = 0;
    }

     switch(currentEvent){
         case 0:
             mHint.setVisibility(View.VISIBLE);
             mCounter.setVisibility(View.VISIBLE);
             mCounter.setTextColor(Color.WHITE);
             pushupCount = 0;
             mHint.setText("Pushup Count");
             mCounter.setText(Integer.toString(pushupCount));
             break;
         case 1:
             mHint.setText("Plank Length");
             mCounter.setVisibility(View.INVISIBLE);
             mChronometer.setVisibility(View.VISIBLE);
             plank();
             break;
         case 2:
             mChronometer.stop();
             mChronometer.setVisibility(View.INVISIBLE);
             jumpingJackCount = 0;
             mHint.setText("Jumping Jack Count");
             mCounter.setText(Integer.toString(jumpingJackCount));
             mCounter.setVisibility(View.VISIBLE);
             startTime = System.currentTimeMillis() - 1000;
             break;
         case 3:
             mHint.setText("Results");
             mCounter.setVisibility(View.INVISIBLE);
             mPushups.setVisibility(View.VISIBLE);
             mPlank.setVisibility(View.VISIBLE);
             mJumpingJacks.setVisibility(View.VISIBLE);

             mPushups.setText("Pushups: " + pushupCount);
             plankCount = (int) ((SystemClock.elapsedRealtime() - mChronometer.getBase()) / 1000);
             mPlank.setText("Plank Time: " + plankCount + " seconds");
             mJumpingJacks.setText("Jumping Jacks: " + jumpingJackCount);

             break;
         case 4:
             mHint.setText("Calculating Power Level");
             mPushups.setVisibility(View.INVISIBLE);
             mPlank.setVisibility(View.INVISIBLE);
             mJumpingJacks.setVisibility(View.INVISIBLE);
             mCounter.setVisibility(View.VISIBLE);
             calculatePowerLevel();
             break;
     }
      return true;
  }



  private void calculatePowerLevel(){
      display = 0;
      powerLevel = 300 * pushupCount + 300 * plankCount + 300 * jumpingJackCount;
      baseTime = System.currentTimeMillis();
      currentEvent = 5;

  }


    private void plank(){
        mBase = mChronometer.getBase();
        mChronometer.setBase(mChronometer.getBase() + SystemClock.elapsedRealtime() - mBase);

        mChronometer.start();

    }


  @Override
  public void onSensorChanged(SensorEvent event){
      if(currentEvent == 0){
          if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
              accelZ = event.values[2] - 9.8;
          }
          //increases compression count by 1 whenever accel_Y passes threshold
          if(accelZ > 5 && accelZ < 6 && System.currentTimeMillis() - startTime >= 1500) {
              if(touchedGround){
                  startTime = System.currentTimeMillis();
                  pushupCount++;
                  mCounter.setText(Integer.toString(pushupCount));
              }
              else
                  touchedGround = true;
          }
      }
      else if(currentEvent == 2){

          if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
              accelY = event.values[1] - 9.8;
          }
          if(accelY > 9 && System.currentTimeMillis() - startTime >= 1000) {
              if(!jumped){
                  startTime = System.currentTimeMillis();
                  jumped = true;
              }
          }
          else if(accelY < -9 && System.currentTimeMillis() - startTime <= 2000 && System.currentTimeMillis() - startTime > 750) {
              jumped = false;
              jumpingJackCount++;
              startTime = System.currentTimeMillis();
              mCounter.setText(Integer.toString(jumpingJackCount));
          }
          else if(System.currentTimeMillis() - startTime > 2000){
              jumped = false;
          }
      }

      if(currentEvent == 5 && display < powerLevel){
              display = (System.currentTimeMillis() - baseTime) * 2;
              mCounter.setText(Long.toString(display));
              if((System.currentTimeMillis() / 500) % 2 == 0){
                  mHint.setVisibility(View.VISIBLE);
              }
              else{
                  mHint.setVisibility(View.INVISIBLE);
              }
              if(display > 9000){
                  mCounter.setTextColor(Color.GREEN);
              }
              else{
                  mCounter.setTextColor(Color.RED);
              }
      }
      else if(currentEvent == 5){
          mHint.setVisibility(View.VISIBLE);
          if(display > 9000){
              if((System.currentTimeMillis() / 250) % 2 == 0){
                  mCounter.setVisibility(View.VISIBLE);
              }
              else{
                  mCounter.setVisibility(View.INVISIBLE);
              }
          }

      }
  }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
        // You must implement this callback in your code.
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



}
